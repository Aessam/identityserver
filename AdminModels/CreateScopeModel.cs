﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Main.IdentityServer4.AdminModels
{
    public class CreateScopeModel
    {

        [Required]
        public string name { get; set; }

        public string description { get; set; }
        [Required]
        public string displayname { get; set; }

    }
}
