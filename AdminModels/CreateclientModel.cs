﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Main.IdentityServer4.AdminModels
{
    public class CreateclientModel
    {

        [Required]
        public string ClientId { get; set; }
        [Required]
        public List<string> scopes { get; set; }




        public string CreateSecret()
        {
            Random random = new Random();

            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 50)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }




}
