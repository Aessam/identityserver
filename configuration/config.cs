﻿using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Main.IdentityServer4.configuration
{
    public static class config
    {
        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId="da8000-02jhgdff-11dhnnc-0147dfyyy-potf48",
                    Username="TestUser",
                    Password="P@ssw0rd",
                    Claims=new List<Claim>
                    {
                        new Claim ("given_name","TEst"),
                        new Claim("family_name","Family")
                    }
                }

            };
        }



        public static IEnumerable<Client> Clients()

        {
            return new List<Client>
                  {
             new Client
               {
            ClientId = "client1",

            // no interactive user, use the clientid/secret for authentication
            AllowedGrantTypes = GrantTypes.ClientCredentials,

            // secret for authentication
            ClientSecrets =
            {
                new Secret("secret".Sha256())
            },

            // scopes that client has access to
            AllowedScopes = { "api1" }
             }
            };
        }


        public static IEnumerable<ApiResource> Apis()
        {
          return  new List<ApiResource>
            {
               new ApiResource("api1","My Api")
            };
        }
    }
}
