﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IdentityServer4.EntityFramework.Interfaces;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Models;
using Main.IdentityServer4.AdminModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Main.IdentityServer4.Controllers
{

    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class AdminController : ControllerBase
    {

        private readonly IConfigurationDbContext _configurationDbContext;

        public AdminController(IConfigurationDbContext configurationDbContext)
        {
            _configurationDbContext = configurationDbContext;
        }

        [Route("admin/createclient")]
        [HttpPost]
        public async Task<IActionResult> Addclients([FromBody]CreateclientModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                string secret = model.CreateSecret();
                var clientobj = new Client
                {
                    ClientId = model.ClientId,

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    // secret for authentication
                    ClientSecrets = { new Secret(secret .Sha256()) },
                    // scopes that client has access to
                    AllowedScopes = model.scopes
                };

                _configurationDbContext.Clients.Add(clientobj.ToEntity());

                await _configurationDbContext.SaveChangesAsync();

                return Created("", new {clientid=clientobj.ClientId , secret=secret ,dbsecret=clientobj.ClientSecrets.FirstOrDefault() });

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        [Route("admin/createscope")]
        [HttpPost]
        public async Task<IActionResult> AddScope([FromBody]CreateScopeModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                    return BadRequest();

                List<Scope> scopes = new List<Scope>();
                #region ResourceScopes
                scopes.Add(new Scope
                {
                    Name = model.name + ".read",
                    DisplayName = model.displayname,
                    Description = model.description
                });
                scopes.Add(new Scope
                {
                    Name = model.name + ".write",
                    DisplayName = model.displayname,
                    Description = model.description
                });
                scopes.Add(new Scope
                {
                    Name = model.name,
                    DisplayName = model.displayname,
                    Description = model.description
                });
           
                #endregion


                var apiResource = new ApiResource
                {
                    Name = model.name,
                    DisplayName = model.displayname,
                    Description = model.description,
                    Scopes =scopes

                };

                _configurationDbContext.ApiResources.Add(apiResource.ToEntity());

             

                await _configurationDbContext.SaveChangesAsync();

                return Created("", apiResource);
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.ToString());
            }

            
        }


    }
}